/* 
 * File:   easyStorage.h
 * Author: Ian
 *
 * Created on 19 maart 2013, 11:40
 */
#ifndef EASYSTORAGE_H
#define EASYSTORAGE_H

typedef struct alarm
{
    int alarmType;
    int hour;
    int minute;
    int seconde;
    int day;
    int month;
    int year;
}alarm_struct;

int initUserData(void);
int savePersistent(alarm_struct *src, int size);
int openPersistent(alarm_struct *src, int size);
void showPage(u_long pgn);

void testRomFs(void);

#endif

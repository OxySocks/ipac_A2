/*! \mainpage SIR firmware documentation
 *
 *  \section intro Introduction
 *  A collection of HTML-files has been generated using the documentation in the sourcefiles to
 *  allow the developer to browse through the technical documentation of this project.
 *  \par
 *  \note these HTML files are automatically generated (using DoxyGen) and all modifications in the
 *  documentation should be done via the sourcefiles.
 */

/*! \file
 *  COPYRIGHT (C) STREAMIT BV 2010
 *  \date 19 december 2003
 */

#define LOG_MODULE              LOG_MAIN_MODULE
#define ETH0_BASE               0xC300
#define ETH0_IRQ                5
#define SYNC_DELAY_MS           30000

/*--------------------------------------------------------------------------*/
/*  Include files                                                           */
/*--------------------------------------------------------------------------*/
#include <stdio.h>
#include <pro/dhcp.h>
#include <string.h>
#include <stdbool.h>
#include <errno.h>
#include <stdlib.h>
#include <time.h>
#include <sys/thread.h>
#include <sys/timer.h>
#include <dev/nicrtl.h>
#include <arpa/inet.h>
#include <dev/board.h>
#include <pro/sntp.h>
#include <sys/confnet.h>
#include <stdio.h>
#include <io.h>
#include <arpa/inet.h>
#include <pro/dhcp.h>
#include <sys/version.h>
#include <dev/irqreg.h>
#include <sys/socket.h>

#include "processStream.h"
#include "startStream.h"
#include "globals.h"
#include "system.h"
#include "portio.h"
#include "display.h"
#include "remcon.h"
#include "sound.h"
#include "keyboard.h"
#include "led.h"
#include "log.h"
#include "uart0driver.h"
#include "mmc.h"
#include "watchdog.h"
#include "flash.h"
#include "spidrv.h"
#include "rtc.h"
#include "easyStorage.h"

/*-------------------------------------------------------------------------*/
/* local routines (prototyping)                                            */
/*-------------------------------------------------------------------------*/
static void SysMainBeatInterrupt(void*);
static void SysControlMainBeat(u_char);
void fallingstart(void);
static bool syncNTPTime(void); 

//tm current, int type, struct alarm arr[]
int InitInet(void);
bool Cansnooze;
int arraySizeB = 0;
struct alarm alarmA;
struct alarm alarmsB[17];
static void checkAlarm(struct alarm arr[]);
static void updateArray(tm, struct alarm arr[]);
void pullDataFromServer(void);

/*-------------------------------------------------------------------------*/
/* Stack check variables placed in .noinit section                         */
/*-------------------------------------------------------------------------*/

/*!
 * \addtogroup System
 */

/*@{*/


/*-------------------------------------------------------------------------*/
/*                         start of code                                   */
/*-------------------------------------------------------------------------*/

/*!
 * \brief ISR MainBeat Timer Interrupt (Timer 2 for Mega128, Timer 0 for Mega256).
 *
 * This routine is automatically called during system
 * initialization.
 *
 * resolution of this Timer ISR is 4,448 msecs
 *
 * \param *p not used (might be used to pass parms from the ISR)
 */
static void SysMainBeatInterrupt(void *p) {

    /*
     *  scan for valid keys AND check if a MMCard is inserted or removed
     */
    KbScan();
    CardCheckCard();
}

/*!
 * \brief Initialise Digital IO
 *  init inputs to '0', outputs to '1' (DDRxn='0' or '1')
 *
 *  Pull-ups are enabled when the pin is set to input (DDRxn='0') and then a '1'
 *  is written to the pin (PORTxn='1')
 */

void SysInitIO(void) {
    /*
     *  Port B:     VS1011, MMC CS/WP, SPI
     *  output:     all, except b3 (SPI Master In)
     *  input:      SPI Master In
     *  pull-up:    none
     */
    outp(0xF7, DDRB);

    /*
     *  Port C:     Address bus
     */

    /*
     *  Port D:     LCD_data, Keypad Col 2 & Col 3, SDA & SCL (TWI)
     *  output:     Keyboard colums 2 & 3
     *  input:      LCD_data, SDA, SCL (TWI)
     *  pull-up:    LCD_data, SDA & SCL
     */
    outp(0x0C, DDRD);
    outp((inp(PORTD) & 0x0C) | 0xF3, PORTD);

    /*
     *  Port E:     CS Flash, VS1011 (DREQ), RTL8019, LCD BL/Enable, IR, USB Rx/Tx
     *  output:     CS Flash, LCD BL/Enable, USB Tx
     *  input:      VS1011 (DREQ), RTL8019, IR
     *  pull-up:    USB Rx
     */
    outp(0x8E, DDRE);
    outp((inp(PORTE) & 0x8E) | 0x01, PORTE);

    /*
     *  Port F:     Keyboard_Rows, JTAG-connector, LED, LCD RS/RW, MCC-detect
     *  output:     LCD RS/RW, LED
     *  input:      Keyboard_Rows, MCC-detect
     *  pull-up:    Keyboard_Rows, MCC-detect
     *  note:       Key row 0 & 1 are shared with JTAG TCK/TMS. Cannot be used concurrent
     */
#ifndef USE_JTAG
    sbi(JTAG_REG, JTD); // disable JTAG interface to be able to use all key-rows
    sbi(JTAG_REG, JTD); // do it 2 times - according to requirements ATMEGA128 datasheet: see page 256
#endif //USE_JTAG

    outp(0x0E, DDRF);
    outp((inp(PORTF) & 0x0E) | 0xF1, PORTF);

    /*
     *  Port G:     Keyboard_cols, Bus_control
     *  output:     Keyboard_cols
     *  input:      Bus Control (internal control)
     *  pull-up:    none
     */
    outp(0x18, DDRG);
}

/* Method to update to update the time with a NTP server
 */
static bool syncNTPTime(void) 
{
    time_t ntp_time = 0;
    tm *ntp_datetime;
    uint32_t ntp_server = inet_addr("185.14.184.13");
    
    if (NutSNTPGetTime(&ntp_server, &ntp_time) == 0) 
    {
        SYNC_STATUS = SYNC_LAST10MIN;

        ntp_datetime = localtime(&ntp_time);
        X12RtcSetClock(ntp_datetime);       

        printf("Synced. (to %02d:%02d:%02d) \n", ntp_datetime->tm_hour, ntp_datetime->tm_min, ntp_datetime->tm_sec);
        return true;
    } 
    else 
    {
        return false;
        puts("Failed to sync.");
    }
}

/*!
 * \brief Starts or stops the 4.44 msec mainbeat of the system
 * \param OnOff indicates if the mainbeat needs to start or to stop
 */
static void SysControlMainBeat(u_char OnOff) {
    int nError = 0;

    if (OnOff == ON) {
        nError = NutRegisterIrqHandler(&OVERFLOW_SIGNAL, SysMainBeatInterrupt, NULL);
        if (nError == 0) {
            init_8_bit_timer();
        }
    } else {
        // disable overflow interrupt
        disable_8_bit_timer_ovfl_int();
    }
}

/* 
 * \AUTHOR Vincent & Ian
 */
void saveAlarm (void)
{
    struct alarm alarm2;
    alarm2.alarmType = 1;
    alarm2.hour = ALARM2_TIME_HOURS;
    alarm2.minute = ALARM2_TIME_MIN;
    alarm2.seconde = ALARM2_TIME_SEC;
    alarm2.day = ALARM2_DATE_DAY;
    alarm2.month = ALARM2_DATE_MONTH;
    alarm2.year = ALARM2_DATE_YEAR;
    alarmsB[arraySizeB] = alarm2;
    arraySizeB++;
    alarm2.alarmType = 3;
    alarmsB[arraySizeB] = alarm2;
    ALARM2_TIME_HOURS = 0;
    ALARM2_TIME_MIN = 0;
    ALARM2_TIME_SEC = 0;
    ALARM2_DATE_DAY = 0;
    ALARM2_DATE_MONTH = 0;
    checkAlarm(alarmsB);
}

/* 
 * \AUTHOR Vincent & Ian
 */
void checkAlarm(struct alarm arr[])
{
    int i = 1;
    bool done = false;
    struct alarm first = arr[0];
    while(!done)
    {
        if(arr[i].alarmType == 3)
        {
            done = true;
        }
        else if(arr[i].year < first.year )
        {            
            first = arr[i];
        }
        else if(arr[i].year == first.year)
        {
            if(arr[i].month < first.month )
            {
                first = arr[i];
            }
            else if(arr[i].month == first.month)
            {
                if(arr[i].day < first.day )
                {
                    first = arr[i];
                }
                else if(arr[i].day == first.day)
                {
                    if(arr[i].hour < first.hour )
                    {
                        first = arr[i];
                    }
                    else if(arr[i].hour == first.hour)
                    {
                      if(arr[i].minute < first.minute )  
                      {
                          first = arr[i];
                      }
                      else if(arr[i].minute == first.minute)
                      {
                          if(arr[i].seconde <= first.seconde )
                          {
                              first = arr[i];
                          }
                      }
                    }
                }
            }
        }
        i++;
    }
    savePersistent((alarm_struct *)alarmsB, sizeof(alarmsB));
    setAlarm(first.alarmType,first.hour,first.minute,first.seconde,first.day,first.month,first.year);
}

/* 
 * \AUTHOR Vincent & Ian
 */
void updateArray(tm current, struct alarm arr[])
{
    int i = 0;
    bool done = false;
    while(!done)
    {
        if(arr[i].alarmType == 3)
        {
            done = true;
            break;
        }
        else if(current.tm_year == arr[i].year &&
                current.tm_mon == arr[i].month && 
                current.tm_mday == arr[i].day && 
                current.tm_hour == arr[i].hour && 
                current.tm_min == arr[i].minute && 
                current.tm_sec == arr[i].seconde )
        {
                arr[i].year += 1;
        }
        i++;
    }
    checkAlarm(arr);
}
/* MenuThread handles the displaying/updating and the buttons of the menu 
 * Any additions to the above parts should probably be put here.
 * 
 * 
 * Author: Daan, Zach & Tycho.
 */
THREAD(ButtonThread, arg) 
{
    for(;;)
    {
        NutSleep(150);
        
        int keyFound = KbGetKeyFound();
        
        if (keyFound == KEY_01)
            ALARM1_STATUS = ALARM_DISABLED;
        if (keyFound == KEY_02)
            ALARM2_STATUS = ALARM_DISABLED;
        if (keyFound == KEY_ALT)
        {
            if(Cansnooze)
            {
                ALARM1_STATUS = ALARM_SNOOZED;
                stop_stream();
                Cansnooze = false;

            }
        }
        
        if(keyFound == KEY_OK + KEY_ESC)
        {  
            SELECTED_MENU = MENU_SETTINGS;
        }                 
        
        if(keyFound == KEY_RIGHT)
        {
            switch(SELECTED_MENU)
            {
                case MENU_ALARM1_TIME:
                case MENU_ALARM1_DATE:                 
                case MENU_ALARM2_TIME:
                case MENU_ALARM2_DATE:
                    switch(MENU_ALARM_SELECTED_TIME)
                    {
                        case MENU_ALARM_SELECTED_1:
                            MENU_ALARM_SELECTED_TIME = MENU_ALARM_SELECTED_2;
                        break;
                        case MENU_ALARM_SELECTED_2:
                            MENU_ALARM_SELECTED_TIME = MENU_ALARM_SELECTED_3;
                        break;                    
                    }
                    break;               
            }                  
        }
        
        if(keyFound == KEY_LEFT)
        {
            switch(SELECTED_MENU)
            {
                case MENU_ALARM1_TIME:
                case MENU_ALARM1_DATE:                        
                case MENU_ALARM2_TIME:
                case MENU_ALARM2_DATE:
                    switch(MENU_ALARM_SELECTED_TIME)
                    {
                        case MENU_ALARM_SELECTED_2:
                            MENU_ALARM_SELECTED_TIME = MENU_ALARM_SELECTED_1;
                        break;
                        case MENU_ALARM_SELECTED_3:
                            MENU_ALARM_SELECTED_TIME = MENU_ALARM_SELECTED_2;
                        break;                    
                    }
                    break;               
            }                  
        }
        
        if (keyFound == KEY_UP)
        {    
            if(CURRENT_VIEW == VIEW_SET_TIMEZONE || SELECTED_MENU == MENU_TIMEZONE_SET)
            {
                if(timezone < 12)
                {
                    timezone++;
                }                 
            }
            else
            {            
                switch(SELECTED_MENU)
                {
                    case MENU_SETTINGS:
                        SELECTED_MENU = MENU_EXTRA;
                        break;
                    case MENU_EXTRA:
                        SELECTED_MENU = MENU_SETTINGS;                    
                        break;                
                    case MENU_RSS:
                        SELECTED_MENU = MENU_WEBDATA;
                        break;
                    case MENU_WEBDATA:
                        SELECTED_MENU = EXTRA_FALLINGASLEEP;
                        break;
                    case EXTRA_FALLINGASLEEP:
                        SELECTED_MENU = MENU_RSS;
                        break;
                    case MENU_ALARM1:
                        SELECTED_MENU = MENU_INIT_INTERNET;
                        break;
                    case MENU_ALARM2:
                        SELECTED_MENU = MENU_ALARM1;
                        break;
                    case MENU_TIMEZONE:
                        SELECTED_MENU = MENU_ALARM2;
                        break;
                    case MENU_INIT_INTERNET:
                        SELECTED_MENU = MENU_TIMEZONE;
                        break;                        
                    case MENU_ALARM1_TIME:
                        switch(MENU_ALARM_SELECTED_TIME)
                        {
                            case MENU_ALARM_SELECTED_1:
                                if(ALARM1_TIME_HOURS < 23)
                                {
                                        ALARM1_TIME_HOURS++;
                                }                              
                                break;
                            case MENU_ALARM_SELECTED_2:
                                if(ALARM1_TIME_MIN < 59)
                                {
                                    ALARM1_TIME_MIN++;                                    
                                }
                                break;
                            case MENU_ALARM_SELECTED_3:
                                if(ALARM1_TIME_SEC < 59)
                                {
                                    ALARM1_TIME_SEC++;                                    
                                }                                
                                break;
                        }                    
                        break;
                    case MENU_ALARM1_DATE:
                        switch(MENU_ALARM_SELECTED_TIME)
                        {
                            case MENU_ALARM_SELECTED_1:
                                if(ALARM1_DATE_DAY < 31)
                                {
                                        ALARM1_DATE_DAY++;
                                }                              
                                break;
                            case MENU_ALARM_SELECTED_2:
                                if(ALARM1_DATE_MONTH < 12)
                                {
                                    ALARM1_DATE_MONTH++;                                    
                                }
                                break;
                            case MENU_ALARM_SELECTED_3:
                                ALARM1_DATE_YEAR++;                                                                                                    
                                break;
                        }                    
                        break;
                    case MENU_ALARM1_SNOOZE:
                        if(ALARM1_SNOOZE < 120)
                        {
                                ALARM1_SNOOZE++;
                        }
                        break;
                    case MENU_ALARM1_SOUND:
                        if(ALARM1_STREAM_CHOICE >= 0 && ALARM1_STREAM_CHOICE <2)
                        {
                            ALARM1_STREAM_CHOICE++;
                        }
                        break;                   
                    case MENU_ALARM2_TIME:
                        switch(MENU_ALARM_SELECTED_TIME)
                        {
                            case MENU_ALARM_SELECTED_1:
                                if(ALARM2_TIME_HOURS < 23)
                                {
                                        ALARM2_TIME_HOURS++;
                                }                              
                                break;
                            case MENU_ALARM_SELECTED_2:
                                if(ALARM2_TIME_MIN < 59)
                                {
                                    ALARM2_TIME_MIN++;                                    
                                }
                                break;
                            case MENU_ALARM_SELECTED_3:
                                if(ALARM2_TIME_SEC < 59)
                                {
                                    ALARM2_TIME_SEC++;                                    
                                }                                
                                break;
                        }                    
                        break;
                    case MENU_ALARM2_DATE:
                        switch(MENU_ALARM_SELECTED_TIME)
                        {
                            case MENU_ALARM_SELECTED_1:
                                if(ALARM2_DATE_DAY < 31)
                                {
                                        ALARM2_DATE_DAY++;
                                }                              
                                break;
                            case MENU_ALARM_SELECTED_2:
                                if(ALARM2_DATE_MONTH < 12)
                                {
                                    ALARM2_DATE_MONTH++;                                    
                                }
                                break;
                            case MENU_ALARM_SELECTED_3:
                                ALARM2_DATE_YEAR++;                                                                                                    
                                break;
                        }                    
                        break;
                    case MENU_ALARM2_SNOOZE:
                        if(ALARM2_SNOOZE < 120)
                        {
                                ALARM2_SNOOZE++;
                        }
                        break;
                    case MENU_ALARM2_SOUND:
                        if(ALARM2_STREAM_CHOICE >= 0 && ALARM2_STREAM_CHOICE <2)
                        {
                            ALARM2_STREAM_CHOICE++;
                        }
                        break;
                }
            }
        }
        
        if (keyFound == KEY_DOWN)
        {   
            if(CURRENT_VIEW == VIEW_SET_TIMEZONE || SELECTED_MENU == MENU_TIMEZONE_SET)
            {
                if(timezone > -12)   
                {
                    timezone--;
                }
            }
            else
            {
                switch(SELECTED_MENU)
                {
                    case MENU_SETTINGS:
                        SELECTED_MENU = MENU_EXTRA;
                        break;
                    case MENU_EXTRA:
                        SELECTED_MENU = MENU_SETTINGS;    
                        break;
                    case MENU_ALARM1:
                        SELECTED_MENU = MENU_ALARM2;
                        break;
                    case MENU_ALARM2:
                        SELECTED_MENU = MENU_TIMEZONE;
                        break;
                    case MENU_TIMEZONE:
                        SELECTED_MENU = MENU_INIT_INTERNET;
                        break;
                    case MENU_INIT_INTERNET:
                        SELECTED_MENU = MENU_ALARM1;
                        break;     
                    case MENU_RSS:
                        SELECTED_MENU = EXTRA_FALLINGASLEEP;
                        break;
                    case EXTRA_FALLINGASLEEP: 
                        SELECTED_MENU = MENU_WEBDATA;
                        break;
                    case MENU_WEBDATA:
                        SELECTED_MENU = MENU_RSS;
                        break;
                    case MENU_ALARM1_TIME:
                        switch(MENU_ALARM_SELECTED_TIME)
                        {
                            case MENU_ALARM_SELECTED_1:
                                if(ALARM1_TIME_HOURS > 0)
                                {
                                        ALARM1_TIME_HOURS--;
                                }                              
                                break;
                            case MENU_ALARM_SELECTED_2:
                                if(ALARM1_TIME_MIN > 0)
                                {
                                    ALARM1_TIME_MIN--;                                    
                                }
                                break;
                            case MENU_ALARM_SELECTED_3:
                                if(ALARM1_TIME_SEC > 0)
                                {
                                    ALARM1_TIME_SEC--;                                    
                                }                                
                                break;
                        }                    
                        break;
                    case MENU_ALARM1_DATE:
                        switch(MENU_ALARM_SELECTED_TIME)
                        {
                            case MENU_ALARM_SELECTED_1:
                                if(ALARM1_DATE_DAY > 0)
                                {
                                        ALARM1_DATE_DAY--;
                                }                              
                                break;
                            case MENU_ALARM_SELECTED_2:
                                if(ALARM1_DATE_MONTH > 0)
                                {
                                    ALARM1_DATE_MONTH--;                                    
                                }
                                break;
                            case MENU_ALARM_SELECTED_3:
                                if(ALARM1_DATE_YEAR > 0)
                                {
                                    ALARM1_DATE_YEAR--;                                    
                                }                                
                                break;
                        }                    
                        break;
                    case MENU_ALARM1_SNOOZE:
                        if(ALARM1_SNOOZE > 0)
                        {
                                ALARM1_SNOOZE--;
                        }
                        break;
                    case MENU_ALARM1_SOUND:
                        if(ALARM1_STREAM_CHOICE > 0 && ALARM1_STREAM_CHOICE <=2)
                        {
                            ALARM1_STREAM_CHOICE--;
                        }
                        break;                 
                    case MENU_ALARM2_TIME:
                        switch(MENU_ALARM_SELECTED_TIME)
                        {
                            case MENU_ALARM_SELECTED_1:
                                if(ALARM2_TIME_HOURS > 0)
                                {
                                        ALARM2_TIME_HOURS--;
                                }                              
                                break;
                            case MENU_ALARM_SELECTED_2:
                                if(ALARM2_TIME_MIN > 0)
                                {
                                    ALARM2_TIME_MIN--;                                    
                                }
                                break;
                            case MENU_ALARM_SELECTED_3:
                                if(ALARM2_TIME_SEC > 0)
                                {
                                    ALARM2_TIME_SEC--;                                    
                                }                                
                                break;
                        }                    
                        break;
                    case MENU_ALARM2_DATE:
                        switch(MENU_ALARM_SELECTED_TIME)
                        {
                            case MENU_ALARM_SELECTED_1:
                                if(ALARM2_DATE_DAY > 0)
                                {
                                        ALARM2_DATE_DAY--;
                                }                              
                                break;
                            case MENU_ALARM_SELECTED_2:
                                if(ALARM2_DATE_MONTH > 0)
                                {
                                    ALARM2_DATE_MONTH--;                                    
                                }
                                break;
                            case MENU_ALARM_SELECTED_3:
                                if(ALARM2_DATE_YEAR > 0)
                                {
                                    ALARM2_DATE_YEAR--;                                    
                                }                                
                                break;
                        }                    
                        break;
                    case MENU_ALARM2_SNOOZE:
                        if(ALARM2_SNOOZE > 0)
                        {
                                ALARM2_SNOOZE--;
                        }
                        break;
                    case MENU_ALARM2_SOUND:
                        if(ALARM2_STREAM_CHOICE > 0 && ALARM2_STREAM_CHOICE <=2)
                        {
                            ALARM2_STREAM_CHOICE--;
                        }
                        break;
                }     
            }
        }
        
        if(keyFound == KEY_OK)
        {
            if(CURRENT_VIEW == VIEW_SET_TIMEZONE)
            {
                At45dbWriteTimezoneToFlash();      
                _timezone = (timezone * -1) * 60 * 60;
                CURRENT_VIEW = VIEW_MAIN_DISPLAY;                
            }             
            
            switch(SELECTED_MENU)
            {
                case MENU_SETTINGS:
                    SELECTED_MENU = MENU_ALARM1;
                    break;
                case MENU_EXTRA:
                    SELECTED_MENU = MENU_RSS;
                    break;
                case MENU_RSS:
                    SELECTED_MENU = MENU_DISPLAY_RSS;
                    break;
                case EXTRA_FALLINGASLEEP:
                    fallingstart();
                    break;
                case MENU_ALARM1:
                    MENU_ALARM_SELECTED_TIME = MENU_ALARM_SELECTED_1;
                    SELECTED_MENU = MENU_ALARM1_TIME;                    
                    break;
                case MENU_ALARM2:
                    MENU_ALARM_SELECTED_TIME = MENU_ALARM_SELECTED_1;
                    SELECTED_MENU = MENU_ALARM2_TIME;
                    break;
                case MENU_INIT_INTERNET:
                    InitInet();
                    break;
                case MENU_TIMEZONE:
                    SELECTED_MENU = MENU_TIMEZONE_SET;                    
                    break;                  
                case MENU_TIMEZONE_SET:
                    At45dbWriteTimezoneToFlash(); 
                    _timezone = (timezone * -1) * 60 * 60;
                    SELECTED_MENU = MENU_TIMEZONE;
                    break;
                case MENU_ALARM1_TIME:
                    ALARM1_DATE_YEAR = 2013;
                    MENU_ALARM_SELECTED_TIME = MENU_ALARM_SELECTED_1;
                    SELECTED_MENU = MENU_ALARM1_DATE;
                    break;
                case MENU_ALARM1_DATE:                    
                    SELECTED_MENU = MENU_ALARM1_SNOOZE;
                    break;
                case MENU_ALARM1_SNOOZE:                    
                    SELECTED_MENU = MENU_ALARM1_SOUND;
                    break;  
                case MENU_ALARM1_SOUND:
                    setAlarm(ALARM_1, ALARM1_TIME_HOURS, ALARM1_TIME_MIN, ALARM1_TIME_SEC, ALARM1_DATE_DAY, ALARM1_DATE_MONTH, ALARM1_DATE_YEAR);
                    SELECTED_MENU = MENU_ALARM1;
                    break;                 
                case MENU_ALARM2_TIME:
                    ALARM2_DATE_YEAR = 2012;
                    MENU_ALARM_SELECTED_TIME = MENU_ALARM_SELECTED_1;
                    SELECTED_MENU = MENU_ALARM2_DATE;
                    break;
                case MENU_ALARM2_DATE:                    
                    SELECTED_MENU = MENU_ALARM2_SNOOZE;
                    break;
                case MENU_ALARM2_SNOOZE:                    
                    SELECTED_MENU = MENU_ALARM2_SOUND;
                    break;
                case MENU_ALARM2_SOUND:
                    setAlarm(ALARM_2, ALARM2_TIME_HOURS, ALARM2_TIME_MIN, ALARM2_TIME_SEC, ALARM2_DATE_DAY, ALARM2_DATE_MONTH, ALARM2_DATE_YEAR);
                    saveAlarm();
                    SELECTED_MENU = MENU_ALARM2;
                    break;
                case MENU_WEBDATA:
                    pullDataFromServer();
                    break;
            }                    
        }       
        
        if(keyFound == KEY_ESC)
        {
            switch(SELECTED_MENU)
            {
                case MENU_SETTINGS:
                    SELECTED_MENU = MENU_CLOSED;  
                    LcdClearBottomLine();
                    break;
                case MENU_EXTRA:
                    SELECTED_MENU = MENU_CLOSED;
                    LcdClearBottomLine();
                    break;   
                case MENU_RSS:
                    SELECTED_MENU = MENU_EXTRA;
                    break;
                case MENU_DISPLAY_RSS:
                    SELECTED_MENU = MENU_RSS;
                    break; 
                case EXTRA_FALLINGASLEEP:
                    SELECTED_MENU = MENU_EXTRA;
                    break;
                case MENU_INIT_INTERNET:
                    SELECTED_MENU = MENU_SETTINGS;
                case MENU_TIMEZONE:
                    SELECTED_MENU = MENU_SETTINGS;
                    break;
                case MENU_TIMEZONE_SET:
                    SELECTED_MENU = MENU_TIMEZONE;
                    break;                    
                case MENU_ALARM1:
                    SELECTED_MENU = MENU_SETTINGS;
                    break;
                case MENU_ALARM1_TIME:
                    SELECTED_MENU = MENU_ALARM1;
                    break;
                case MENU_ALARM1_DATE:
                    MENU_ALARM_SELECTED_TIME = MENU_ALARM_SELECTED_1;
                    SELECTED_MENU = MENU_ALARM1_TIME;
                    break;
                case MENU_ALARM1_SNOOZE:
                    MENU_ALARM_SELECTED_TIME = MENU_ALARM_SELECTED_1;
                    SELECTED_MENU = MENU_ALARM1_DATE;
                    break;
                case MENU_ALARM1_SOUND:                    
                    SELECTED_MENU = MENU_ALARM1_SNOOZE;
                    break;
                case MENU_ALARM2:
                    SELECTED_MENU = MENU_SETTINGS;
                    break;
                case MENU_ALARM2_TIME:
                    SELECTED_MENU = MENU_ALARM2;
                    break;
                case MENU_ALARM2_DATE:
                    MENU_ALARM_SELECTED_TIME = MENU_ALARM_SELECTED_1;
                    SELECTED_MENU = MENU_ALARM2_TIME;
                    break;
                case MENU_ALARM2_SNOOZE:
                    MENU_ALARM_SELECTED_TIME = MENU_ALARM_SELECTED_1;
                    SELECTED_MENU = MENU_ALARM2_DATE;
                    break;
                case MENU_ALARM2_SOUND:                    
                    SELECTED_MENU = MENU_ALARM2_SNOOZE;
                    break;
                case MENU_WEBDATA:
                    SELECTED_MENU = MENU_EXTRA;
                    break;
            }                 
        } 
    } 
}

/* Thread that handles all alarm logic
 * This includes pausing the sound after 5 seconds and
 * the actual triggering of the sound
 * 
 * \AUTHOR Oxysocks
 */
THREAD(AlarmThread, arg)
{
    u_long alarm1Status = 0;
    u_long alarm2Status = 0;

    int alarmCounter = 0;
    bool alarmIsTriggered = false;

    for (;;) {
        NutSleep(1000);

        X12RtcGetStatus(&alarm1Status);
        //alarm2Status = alarm1Status;

        alarm1Status &= 0b00100000;
        alarm2Status &= 0b01000000;
        
        //printf("Alarm1: %lu \n", alarm1Status);
        //printf("Alarm2: %lu \n", alarm2Status);
        
        if(ALARM1_STATUS == ALARM_SNOOZED && alarmIsTriggered)
        {
                puts("alarm 1 snoozed");
                u_long sleeptime = (ALARM1_SNOOZE *60000);
                NutSleep(sleeptime);
                start_playing_radio2();
                ALARM1_STATUS = ALARM_DISABLED;
                alarmIsTriggered = false;
        }
        if (alarm1Status != 0 && (ALARM1_STATUS == ALARM_ENABLED)) {
            //triggerAlarm();
            tm alarm1,current;
            getAlarm(ALARM_1, &alarm1);
            X12RtcGetClock(&current);
            if(current.tm_wday > 0 && current.tm_wday < 6)
            {
                if(current.tm_yday != 0 || current.tm_yday != 120 || current.tm_yday != 360 || current.tm_yday != 359)
                {
                    if(ALARM1_STREAM_CHOICE ==0)
                        {
                            start_playing_radio2();
                        }
                        else if (ALARM1_STREAM_CHOICE ==1)
                        {
                            start_playing_JPOP();
                        }
                        else if (ALARM1_STREAM_CHOICE ==2)
                        {
                            triggerAlarm();
                        }             
                }
            }
            puts("Alarm 1 triggered");
            X12RtcClearStatus(0b00100000);
            alarm1.tm_mday += 1;
            if(alarm1.tm_mday == 31)
            {
                alarm1.tm_mday = 0;
                alarm1.tm_mon += 1;
                if(alarm1.tm_mon == 11)
                {
                    alarm1.tm_mon = 0;
                    alarm1.tm_year += 1;
                }
            }
            setAlarm(0,alarm1.tm_hour,alarm1.tm_min,alarm1.tm_sec,alarm1.tm_mday,alarm1.tm_mon,alarm1.tm_year);
            alarmIsTriggered = true;
            ALARM1_STATUS = ALARM_DISABLED;
        }

        if (alarm2Status != 0 && (ALARM2_STATUS == ALARM_ENABLED)) {
            tm alarm2;
            getAlarm(ALARM_2, &alarm2);
            if(ALARM2_STREAM_CHOICE ==0)
            {
                start_playing_radio2();
            }
            else if (ALARM2_STREAM_CHOICE ==1)
            {
                start_playing_JPOP();
            }
            else if (ALARM2_STREAM_CHOICE ==2)
            {
                triggerAlarm();
            }  
            puts("Alarm 2 triggered");
            X12RtcClearStatus(0b01000000);
            
            updateArray(alarm2, alarmsB);
            
            alarmIsTriggered = true;
            ALARM2_STATUS = ALARM_DISABLED;
        }

        if (alarmIsTriggered)
            alarmCounter++;
        else
            alarmCounter = 0;
    }
}

/* Thread that handles sync with the NTP server.
 * \AUTHOR Oxysocks
 */
THREAD(TimeSyncThread, arg)
{
    for(;;) 
    {
        NutSleep(SYNC_DELAY_MS);
        
        if (!syncNTPTime())
            puts("Sync failed");
    }
}

/* 
 * \AUTHOR Daan
 */
THREAD(DisplayThread, arg)
{
    for(;;)
    {
        NutSleep(250); 
          
        //SHOW WELCOME MESSAGE
        if(CURRENT_VIEW == VIEW_WELCOME_MESSAGE)
        {
            LcdDisplayWelcomeMessage();                    
        }
        //SHOW TIMEZONE SETTING
        else if(CURRENT_VIEW == VIEW_SET_TIMEZONE)
        {            
            LcdDisplayTimezoneSet();
        }      
        else if(CURRENT_VIEW == VIEW_MAIN_DISPLAY)
        {
            LcdDisplayTime();
            LcdDisplayMenu();
        }
        
        LcdUpdate();
    }
}

/* Retrieves RSS feed data from PHP page
 * located on a webserver which retrieves RSS data 
 * from another feed.
 * \AUTHOR Zack
 */
THREAD(RSSThread, arg) 
{
    int threadRan = 0;
    for (;;) {
        NutSleep(250); 
        if (threadRan == 0) {
            FILE *rss;

            char *serverData;

            TCPSOCKET *sock;
            sock = NutTcpCreateSocket();
            if (NutTcpConnect(sock, inet_addr("84.84.159.24"), 80)) {
                LogMsg_P(LOG_ERR, PSTR("Error: >> NutTcpConnect()"));
            }

            rss = _fdopen((int) sock, "r+b");
            fprintf(rss, "GET %s HTTP/1.0\r\n", "/IPAC_website/rssparse.php");
            fprintf(rss, "Host: %s\r\n", "84.84.159.24");
            fprintf(rss, "User-Agent: Ethernut\r\n");
            fprintf(rss, "Accept: */*\r\n");
            fprintf(rss, "Icy-MetaData: 1\r\n");
            fprintf(rss, "Connection: close\r\n\r\n");

            fflush(rss);
            serverData = (char *) malloc(1024 * sizeof (char));
            while (fgets(serverData, 1024, rss)) {
                if (0 == *serverData)
                    break;
            }
            strcat(RSS_DATA, serverData);
            printf("%s\n", RSS_DATA);
            threadRan = 1;
        }
    }    
}

/* Method to initialise the internet connection.
 * Prints the 'network' settings.
 * \AUTHOR Tim
 */
int InitInet(void) 
{
    u_long baud = 115200;

    NutRegisterDevice(&DEV_DEBUG, 0, 0);
    freopen(DEV_DEBUG_NAME, "w", stdout);
    _ioctl(_fileno(stdout), UART_SETSPEED, &baud);

    if (NutRegisterDevice(&DEV_ETHER, 0, 0)) {
        puts("Registering " DEV_ETHER_NAME " failed.");
    }
    else if (NutDhcpIfConfig(DEV_ETHER_NAME, NULL, 0)) {
        puts("Configuring " DEV_ETHER_NAME " failed.");
    }

    //Print the current network settings.
    printf("\nConnected to the network (%s).\n", inet_ntoa(confnet.cdn_ip_addr));
    printf("Settings:\n");
    printf("IF Name: %s \n", confnet.cd_name);
    printf("IP Addr: %s \n", inet_ntoa(confnet.cdn_ip_addr));
    printf("IP Mask: %s \n", inet_ntoa(confnet.cdn_ip_mask));
    printf("Gateway: %s \n", inet_ntoa(confnet.cdn_gateway));
    return 0;
}

/* initAll - Method to get rid of all the init junk from the main.
 * only needed PRIVATE and (if properly coding) only ONCE.
 * for further info check the main() info.
 * 
 * Author: STREAMIT
 * Moved by Oxysocks.
 */
void initAll(void) 
{
    WatchDogDisable();
    NutDelay(100);
    SysInitIO();
    SPIinit();
    LedInit();
    LcdLowLevelInit();
    Uart0DriverInit();
    Uart0DriverStart();
    LogInit();
    CardInit();
    X12Init();
    At45dbInit();
    RcInit();
    KbInit();
    initSound();
    InitInet();

    openPersistent((alarm_struct *)alarmsB, sizeof(alarmsB));
    SysControlMainBeat(ON);     // Enable 4.4 msecs hartbeat interrupt
    NutThreadSetPriority(1);    // Increase our priority so we can feed the watchdog.
    sei();                      // Enable global interrupts
}


/*!
 * \brief Main method
 */
int main(void) 
{
    //INITIALISE THE ENTIRE ETHERNUT/STREAMIT BLOAT.
    initAll();
    ALARM1_SNOOZE = 20;
    Cansnooze= true;
    setVolume(MAX_VOLUME, MAX_VOLUME);

    //START THE TIME DISPLAYING THREAD
    NutThreadCreate("Displaying Thread", DisplayThread, NULL, 512);   
    NutThreadCreate("Button Thread", ButtonThread, NULL, 512); 
    NutThreadCreate("Time Sync", TimeSyncThread, NULL, 512);
    NutThreadCreate("Alarm Thread", AlarmThread, NULL, 512);
    NutThreadCreate("RSS Thread", RSSThread, NULL, 512);
    ALARM1_STATUS = ALARM_ENABLED;
    ALARM2_STATUS = ALARM_ENABLED;
   
    tm gmt;
    gmt.tm_hour = 10;
    gmt.tm_min = 36;
    gmt.tm_sec = 10;
    gmt.tm_mday = 4;
    gmt.tm_mon = 3;
    gmt.tm_year = 113;
    
    setTimeWithStruct(&gmt);
    
    gmt.tm_sec += 5;
    setAlarmA(&gmt);
    
    //setTime(10, 36, 55, 4, 3, 113);
    //setAlarm(ALARM_1, 10, 37, 01, 4, 3, 113);   
    
    //CLEARFLASH
//    int intToWrite = 0;
//    char buffer[10];
//    buffer[0] = intToWrite;
//    buffer[1] = intToWrite >> 8;
//
//    At45dbPageWrite(0, buffer, sizeof (int)); 
          
    //TURN ON THE BACKLIGHT
    LcdBackLight(LCD_BACKLIGHT_ON);  
       
    //MAIN APPLICATION LOOP
    //CURRENTLY ONLY USED FOR DEBUGGING/TESTING
    // :)
    for (;;) 
    {
        //WAIT. GIVE THIS 14MHZ BEAST SOME TIME.
        NutSleep(1000);

        tm time;
        X12RtcGetClock(&time);
        //printf("RTC: %02d:%02d:%02d \n", time.tm_hour, time.tm_min, time.tm_sec);

        tm alarm1;
        getAlarm(ALARM_1, &alarm1);
        //printf("ALARM1: %02d:%02d:%02d dag: %02d \n", alarm1.tm_hour, alarm1.tm_min, alarm1.tm_sec, alarm1.tm_mday);
 
        tm alarm2;
        getAlarm(ALARM_2, &alarm2);
        //printf("ALARM2: %02d:%02d:%02d \n", alarm2.tm_hour, alarm2.tm_min, alarm2.tm_sec);
        
        //printf("uur:%d min:%d sec:%d \n",alarmsB[0].hour, alarmsB[0].minute, alarmsB[0].seconde);
        //printf("uur:%d min:%d sec:%d \n",alarmsB[1].hour, alarmsB[1].minute, alarmsB[1].seconde);
        //printf("uur:%d min:%d sec:%d \n",alarmsB[2].hour, alarmsB[2].minute, alarmsB[2].seconde);
        //printf("uur:%d min:%d sec:%d \n",alarmsArr[3].hour, alarmsArr[3].minute, alarmsArr[3].seconde);
    }
   
    //RESTART SNOOP DOGG OR SUMTHIN.
    //GOT TO HAVE SOME FUN WHILE CODING AMIRITE.
    //This stupid thing crashes for no reason at all, NO REASON AT ALL.
    WatchDogRestart();
    return (0);
}

/* 
 * Author: Tycho
 */
static int fallingstarted;
THREAD(FallingThread, arg)
{
    int volume = 0;
    setVolume(volume,volume);
    start_playing_radio2();

    for(;;)
    {
        NutSleep(1000);

        if (volume <=100)
        {
                setVolume(volume,volume);
                volume++;
        }
        else
        {
            fallingstarted = 0;
            stop_stream();
            NutThreadExit();
            for (;;);
        }
        
    }
}

/* 
 * Author: Tycho
 */
void fallingstart(void)
{
    if(fallingstarted !=1)
    {
        NutThreadCreate("LowerVolumeThread", FallingThread, NULL, 512);
        fallingstarted = 1;
    }
}

/*
 * Method to pull data from webserver
 * Author: Daan
 */
void pullDataFromServer(void)
{
    FILE *webdata;
    
    char *data;

    TCPSOCKET *sock;

    sock = NutTcpCreateSocket();

    if(NutTcpConnect(sock, inet_addr("84.84.159.24"), 80))
    {
        LogMsg_P(LOG_ERR, PSTR("Error: >> NutTcpConnect()"));
    }

    webdata = _fdopen((int)sock, "r+b");

    fprintf(webdata, "GET %s HTTP/1.0\r\n", "/ipac_website/allsettings.php");
    fprintf(webdata, "Host: %s\r\n", "server.daanbroekhuizen.com");
    fprintf(webdata, "User-Agent: Ethernut\r\n");
    fprintf(webdata, "Accept: */*\r\n");
    fprintf(webdata, "Icy-MetaData: 1\r\n");
    fprintf(webdata, "Connection: close\r\n\r\n");
    fflush(webdata);

    data = (char *) malloc(1024 * sizeof(char));   
 
    while(fgets(data, 1024, webdata))
    {
        if(0 == *data)
        {
            puts("No data!");
            break;   
        }
    }  
    
    printf("%s", data);
    
    free(data);
    free(webdata);
    
    NutTcpCloseSocket(sock);
}



/* ---------- end of module ------------------------------------------------ */

/*@}*/

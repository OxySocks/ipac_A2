/* ========================================================================
 * [PROJECT]    SIR100
 * [MODULE]     Display
 * [TITLE]      display source file
 * [FILE]       display.c
 * [VSN]        1.0
 * [CREATED]    26092003
 * [LASTCHNGD]  06102006
 * [COPYRIGHT]  Copyright (C) STREAMIT BV
 * [PURPOSE]    contains all interface- and low-level routines to
 *              control the LCD and write characters or strings (menu-items)
 * ======================================================================== */

#define LOG_MODULE  LOG_DISPLAY_MODULE

#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include <sys/types.h>
#include <sys/timer.h>
#include <sys/event.h>
#include <sys/thread.h>
#include <sys/heap.h>

#include "flash.h"
#include "globals.h"
#include "system.h"
#include "portio.h"
#include "display.h"
#include "log.h"
#include "rtc.h"

/*-------------------------------------------------------------------------*/
/* local defines                                                           */
/*-------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------*/
/* local variable definitions                                              */
/*-------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/
/* local routines (prototyping)                                            */
/*-------------------------------------------------------------------------*/
static void LcdWriteByte(u_char, u_char);
static void LcdWriteNibble(u_char, u_char);
static void LcdWaitBusy(void);

extern int SYNC_STATUS;
extern int ALARM1_STATUS;
extern int ALARM2_STATUS;

extern int SELECTED_MENU;

static char *bottomline;
static char *bottomline;
static int bottomcount = 0;

void LcdWriteBottomLine(char input, int cursorPosition);
void LcdClearBottomLine(void);

/*!
 * \addtogroup Display
 */

/*@{*/

/*-------------------------------------------------------------------------*/
/*                         start of code                                   */
/*-------------------------------------------------------------------------*/

/* ����������������������������������������������������������������������� */
/*!
 * \brief control backlight
 */
/* ����������������������������������������������������������������������� */
void LcdBackLight(u_char Mode)
{
    if (Mode==LCD_BACKLIGHT_ON)
    {
        sbi(LCD_BL_PORT, LCD_BL_BIT);   // Turn on backlight
    }

    if (Mode==LCD_BACKLIGHT_OFF)
    {
        cbi(LCD_BL_PORT, LCD_BL_BIT);   // Turn off backlight
    }
}

/* ����������������������������������������������������������������������� */
/*!
 * \brief Write a single character on the LCD
 *
 * Writes a single character on the LCD on the current cursor position
 *
 * \param LcdChar character to write
 */
/* ����������������������������������������������������������������������� */
void LcdChar(char MyChar)
{
    LcdWriteByte(WRITE_DATA, MyChar);
}

/* ����������������������������������������������������������������������� */
/*!
 * \brief Low-level initialisation function of the LCD-controller
 *
 * Initialise the controller and send the User-Defined Characters to CG-RAM
 * settings: 4-bit interface, cursor invisible and NOT blinking
 *           1 line dislay, 10 dots high characters
 *
 */
/* ����������������������������������������������������������������������� */
 void LcdLowLevelInit()
{
    u_char i;

    NutDelay(140);                               // wait for more than 140 ms after Vdd rises to 2.7 V

    for (i=0; i<3; ++i)
    {
        LcdWriteNibble(WRITE_COMMAND, 0x33);      // function set: 8-bit mode; necessary to guarantee that
        NutDelay(4);                              // SIR starts up always in 5x10 dot mode
    }

    LcdWriteNibble(WRITE_COMMAND, 0x22);        // function set: 4-bit mode; necessary because KS0070 doesn't
    NutDelay(1);                                // accept combined 4-bit mode & 5x10 dot mode programming

    //LcdWriteByte(WRITE_COMMAND, 0x24);        // function set: 4-bit mode, 5x10 dot mode, 1-line
    LcdWriteByte(WRITE_COMMAND, 0x28);          // function set: 4-bit mode, 5x7 dot mode, 2-lines
    NutDelay(5);

    LcdWriteByte(WRITE_COMMAND, 0x0C);          // display ON/OFF: display ON, cursor OFF, blink OFF
    NutDelay(5);

    LcdWriteByte(WRITE_COMMAND, 0x01);          // display clear
    NutDelay(5);

    LcdWriteByte(WRITE_COMMAND, 0x06);          // entry mode set: increment mode, entire shift OFF


    LcdWriteByte(WRITE_COMMAND, 0x80);          // DD-RAM address counter (cursor pos) to '0'
}


/* ����������������������������������������������������������������������� */
/*!
 * \brief Low-level routine to write a byte to LCD-controller
 *
 * Writes one byte to the LCD-controller (by  calling LcdWriteNibble twice)
 * CtrlState determines if the byte is written to the instruction register
 * or to the data register.
 *
 * \param CtrlState destination: instruction or data
 * \param LcdByte byte to write
 *
 */
/* ����������������������������������������������������������������������� */
static void LcdWriteByte(u_char CtrlState, u_char LcdByte)
{
    LcdWaitBusy();                      // see if the controller is ready to receive next byte
    LcdWriteNibble(CtrlState, LcdByte & 0xF0);
    LcdWriteNibble(CtrlState, LcdByte << 4);

}

/* ����������������������������������������������������������������������� */
/*!
 * \brief Low-level routine to write a nibble to LCD-controller
 *
 * Writes a nibble to the LCD-controller (interface is a 4-bit databus, so
 * only 4 databits can be send at once).
 * The nibble to write is in the upper 4 bits of LcdNibble
 *
 * \param CtrlState destination: instruction or data
 * \param LcdNibble nibble to write (upper 4 bits in this byte
 *
 */
/* ����������������������������������������������������������������������� */
static void LcdWriteNibble(u_char CtrlState, u_char LcdNibble)
{
    outp((inp(LCD_DATA_DDR) & 0x0F) | 0xF0, LCD_DATA_DDR);  // set data-port to output again

    outp((inp(LCD_DATA_PORT) & 0x0F) | (LcdNibble & 0xF0), LCD_DATA_PORT); // prepare databus with nibble to write

    if (CtrlState == WRITE_COMMAND)
    {
        cbi(LCD_RS_PORT, LCD_RS);     // command: RS low
    }
    else
    {
        sbi(LCD_RS_PORT, LCD_RS);     // data: RS high
    }

    sbi(LCD_EN_PORT, LCD_EN);

    asm("nop\n\tnop");                    // small delay

    cbi(LCD_EN_PORT, LCD_EN);
    cbi(LCD_RS_PORT, LCD_RS);
    outp((inp(LCD_DATA_DDR) & 0x0F), LCD_DATA_DDR);           // set upper 4-bits of data-port to input
    outp((inp(LCD_DATA_PORT) & 0x0F) | 0xF0, LCD_DATA_PORT);  // enable pull-ups in data-port
}

/* ����������������������������������������������������������������������� */
/*!
 * \brief Low-level routine to see if the controller is ready to receive
 *
 * This routine repeatetly reads the databus and checks if the highest bit (bit 7)
 * has become '0'. If a '0' is detected on bit 7 the function returns.
 *
 */
/* ����������������������������������������������������������������������� */
static void LcdWaitBusy()
{
    u_char Busy = 1;
	u_char LcdStatus = 0;

    cbi (LCD_RS_PORT, LCD_RS);              // select instruction register

    sbi (LCD_RW_PORT, LCD_RW);              // we are going to read

    while (Busy)
    {
        sbi (LCD_EN_PORT, LCD_EN);          // set 'enable' to catch 'Ready'

        asm("nop\n\tnop");                  // small delay
        LcdStatus =  inp(LCD_IN_PORT);      // LcdStatus is used elsewhere in this module as well
        Busy = LcdStatus & 0x80;            // break out of while-loop cause we are ready (b7='0')
    }

    cbi (LCD_EN_PORT, LCD_EN);              // all ctrlpins low
    cbi (LCD_RS_PORT, LCD_RS);
    cbi (LCD_RW_PORT, LCD_RW);              // we are going to write
}

/*!
 * LcdReset
 * Method to reset the entire display.
 * Everything on it will be cleared.
 * 
 * \AUTHOR Oxysocks
 */
void LcdReset()
{
    LcdWriteByte(WRITE_COMMAND, COMMAND_CLEAR_SCREEN);
}

void LcdGoToFirstLine()
{
    LcdWriteByte(WRITE_COMMAND, 0x80);
}

/*!
 * LcdDisplayTime
 * Helper method to set the time to the current time.
 * Split because any changes in 'logic' will be seperated
 * from the actual displaying.
 * 
 * \AUTHOR Oxysocks
 */
void LcdDisplayTime(void) 
{
    //Get the latest clock
    tm gmt;
    X12RtcGetClock(&gmt);
    //Buffer for the time.
    char timeStringBuffer[10];
    //Put the formatted time into the time buffer.
    sprintf(timeStringBuffer, "%02d:%02d %02d/%02d", gmt.tm_hour, gmt.tm_min, gmt.tm_mday, gmt.tm_mon);
    //Write the time buffer to the LCD
    LcdTimeString(SYNC_STATUS, timeStringBuffer, ALARM1_STATUS, ALARM2_STATUS);
}

/*!
 * LcdTimeString(int, const char*, int alarm1Status, int alarm2Status)
 * \NOTICE: Please use defined variables ALARM_ENABLED & ALARM_DISABLED. Provides clarity.
 * \NOTICE: Long method, but should be pretty damned accurate.
 * 
 * \BRIEF 
 * Method to handle the time-string being displayed.
 * Basicly does everything for you.
 * If anything goes wrong on the first line; take a look at this method.
 * \ENDBRIEF
 * 
 * \AUTHOR: Oxysocks
 * 
 * \PARAM: syncStatus, input for the current synchronisation status:
 * 0: SYNCED LAST 10 MIN,
 * 1: 1H > SYNCED > 10MIN,
 * 2/other: SYNCED > 1 HOUR.
 * 
 * \PARAM: timeString, input for the current timeString.
 * 
 * \PARAM: alarm1Status, input for the current status of alarm 1. 
 * \PARAM: alarm2Status, input for the current status of alarm 2.
 * 
 * 
 * \RETURN: void.
 */
bool blink = true; // Variable that is used for blinking.

void LcdTimeString(int syncStatus, const char* timeString, int alarm1Status, int alarm2Status)
{    
    //Reset the LCD.
    LcdClearTopLine();
    
    //Set the cursor to the first char of the first line.
    LcdWriteByte(WRITE_COMMAND, 0x80);
    
    //Handle the 'SYNC' display message.
    //If synced in the last 10 minutes.
    if(syncStatus == SYNC_LAST10MIN)
    {
        LcdWriteByte(WRITE_DATA, CHAR_BLACKSQUARE);
    }
    //If synced in the last hour, but not the last 10 min.
    else if(syncStatus == SYNC_LASTHOUR)
    {
        //If we should blink, show the square.
        //Otherwise nothing.
        if(blink)
            LcdWriteByte(WRITE_DATA, CHAR_BLACKSQUARE);
        else
            LcdWriteByte(WRITE_DATA, ' ');
        
        //Toggle the 'blink' speed.
        blink = !blink;
    }
    //If sync is delayed for more than 1 hour.
    else if(syncStatus == SYNC_DELAYED)
    {
        LcdWriteByte(WRITE_DATA, ' ');
    }
    
    //Initialise i.
    int i = 0;
    
    //Write the 'timestamp', inputted by the thread that updates
    //This part of the code. See main.c for the thread code.
    for(; i < strlen(timeString); i++)
    {
        LcdWriteByte(WRITE_DATA, timeString[i]);
    }
    
    //Write an empty char - for spacing
    LcdWriteByte(WRITE_DATA, ' ');
    LcdWriteByte(WRITE_DATA, ' ');

    //Write the status of the first alarm to the display.
    if(alarm1Status == ALARM_ENABLED)
        LcdWriteByte(WRITE_DATA, '1');
    else
        LcdWriteByte(WRITE_DATA, ' ');
   
    //LcdWriteByte(WRITE_COMMAND, 0xBC);
    //Write the status of the second alarm to the display.
    if(alarm2Status == ALARM_ENABLED)
        LcdWriteByte(WRITE_DATA, '2');
    else
        LcdWriteByte(WRITE_DATA, ' ');
}

/*!
 * LcdDisplayMenu
 * Method to show the menu.
 * \AUTHOR Daan
 */
void LcdDisplayMenu() 
{    
    if (SELECTED_MENU != MENU_CLOSED) {    
        char buffer[100];

        switch (SELECTED_MENU) {
            case MENU_SETTINGS:
                LCDSetBottomLine("Instellingen");                
                break;
            case MENU_EXTRA:
                LCDSetBottomLine("Extra");
                break; 
            case MENU_RSS:
                LCDSetBottomLine("RSS");
                break; 
            case MENU_DISPLAY_RSS:
                LCDSetBottomLine(RSS_DATA);
                break; 
            case MENU_INIT_INTERNET:
                LCDSetBottomLine("Connect Internet");
                break;
            case MENU_TIMEZONE:
                LCDSetBottomLine("Tijdzone");
                break;
            case MENU_TIMEZONE_SET:
                if(timezone >= 0)                
                    sprintf(buffer, "Tijdzone: GMT+%i", timezone);                
                else
                    sprintf(buffer, "Tijdzone: GMT%i", timezone);                   
                LCDSetBottomLine(buffer);               
                break;
            case MENU_ALARM1:
                LCDSetBottomLine("Alarm 1"); 
                break;
            case MENU_ALARM1_TIME:
                sprintf(buffer, "Tijd: %02d:%02d:%02d", ALARM1_TIME_HOURS, ALARM1_TIME_MIN, ALARM1_TIME_SEC);
                LCDSetBottomLine(buffer);
                break;           
            case MENU_ALARM1_DATE:          
                sprintf(buffer, "Datum: %02d/%02d/%02d", ALARM1_DATE_DAY, ALARM1_DATE_MONTH, ALARM1_DATE_YEAR % 100);
                LCDSetBottomLine(buffer);        
                break;
            case MENU_ALARM1_SNOOZE:
                sprintf(buffer, "Snooze: %d", ALARM1_SNOOZE);       
                LCDSetBottomLine(buffer);   
                break;
            case MENU_ALARM1_SOUND:
                if(ALARM1_STREAM_CHOICE ==0)
                        {
                            LCDSetBottomLine("Radio 2");
                        }
                        else if (ALARM1_STREAM_CHOICE ==1)
                        {
                            LCDSetBottomLine("Heavy metal");
                        }
                        else if (ALARM1_STREAM_CHOICE == 2) 
                        {
                            LCDSetBottomLine("Beeb");
                        }
                
                break;     
            case MENU_ALARM2:
                LCDSetBottomLine("Alarm 2");
                break;
            case MENU_ALARM2_TIME:
                sprintf(buffer, "Tijd: %02d:%02d:%02d", ALARM2_TIME_HOURS, ALARM2_TIME_MIN, ALARM2_TIME_SEC);
                LCDSetBottomLine(buffer); 
                break;
            case MENU_ALARM2_DATE:
                sprintf(buffer, "Datum: %02d/%02d/%02d", ALARM2_DATE_DAY, ALARM2_DATE_MONTH, ALARM2_DATE_YEAR % 100);                               
                LCDSetBottomLine(buffer); 
                break;
            case MENU_ALARM2_SNOOZE:
                sprintf(buffer, "Snooze: %d", ALARM2_SNOOZE);
                LCDSetBottomLine(buffer);                 
                break;
            case MENU_ALARM2_SOUND:
                if(ALARM2_STREAM_CHOICE ==0)
                        {
                            LCDSetBottomLine("Radio 2");
                        }
                        else if (ALARM2_STREAM_CHOICE ==1)
                        {
                            LCDSetBottomLine("Heavy metal");
                        }
                        else if (ALARM2_STREAM_CHOICE ==2)
                        {
                            LCDSetBottomLine("Beeb");
                        }
                break;
            case EXTRA_FALLINGASLEEP:
                LCDSetBottomLine("Falling a sleep");
                break;
            case MENU_WEBDATA:
                LCDSetBottomLine("Webdata Downloaden");
                break;
        }
    } 
    else
        LCDSetBottomLine("");
}

/*!
 * LcdClearBottomLine
 * Method to write to the top line.
 * 
 * \PARAM input                 Character that should be written to the top line.
 * \PARAM cursorPosition        Position at which to write the character.
 */
void LcdWriteTopLine(char input, int cursorPosition)
{
    LcdWriteByte(WRITE_COMMAND, FIRSTPOS_LINE_0 + cursorPosition);   

    LcdWriteByte(WRITE_DATA, input);    
}

/*!
 * LcdClearBottomLine
 * Method to clear the top line.
 * \AUTHOR Daan
 */
void LcdClearTopLine() 
{
    int i;

    for (i = 0; i < 16; i++) {
        LcdWriteByte(WRITE_COMMAND, FIRSTPOS_LINE_0 + i);

        LcdWriteByte(WRITE_DATA, ' ');
    }
}

/*!
 * LcdClearBottomLine
 * Method to clear the bottom line.
 * \AUTHOR Daan
 */
void LcdClearBottomLine()
{        
    int i;
    
    for(i = 0; i < 16; i++)
    {
        LcdWriteByte(WRITE_COMMAND, FIRSTPOS_LINE_1 + i);  
        LcdWriteByte(WRITE_DATA, ' ');                
    }
}

/* 
 * \AUTHOR Daan
 */
void LcdDisplayWelcomeMessage(void)
{
    char _welcomeTopMessage[16] = "   Welcome by   ";       
    
    int i;      
      
    for(i = 0; i < sizeof(_welcomeTopMessage); i++)
    {
        LcdWriteTopLine(_welcomeTopMessage[i], i);        
    }    
    
    LCDSetBottomLine("   JTI-2.3-A2   ");
    LcdUpdate();
    
    NutSleep(3000);      
    
    LcdClearTopLine();
  
    if(At45dbTimezoneExistsOnFlash() == 1)
    {
        char byteArrayToRieceve[2];
        
        if(At45dbPageRead(1, &byteArrayToRieceve, sizeof(int)) == 0)
        {
            timezone = byteArrayToRieceve[0];            
            _timezone = (timezone * -1) * 60 * 60;
            CURRENT_VIEW = VIEW_MAIN_DISPLAY;
        }
        else
            CURRENT_VIEW = VIEW_SET_TIMEZONE;             
    }
    else
        CURRENT_VIEW = VIEW_SET_TIMEZONE;    
}

/* 
 * \AUTHOR Daan
 */
void LcdDisplayTimezoneSet(void)
{
    char _text[13] = "Tijdzone: GMT";    

    LcdClearTopLine();

    int i;

    for(i = 0; i < sizeof(_text); i++)
    {
        LcdWriteTopLine(_text[i], i);            
    }

    char buffer[10];

    sprintf(buffer, "%i", timezone);

    if(timezone >= 0 && timezone <= 9)
    {
        LcdWriteTopLine('+', sizeof(_text));      
        LcdWriteTopLine(buffer[0], sizeof(_text) + 1);          
    }
    else if(timezone >= -9 && timezone <= -1)
    {        
        LcdWriteTopLine(buffer[0], sizeof(_text));
        LcdWriteTopLine(buffer[1], sizeof(_text) + 1);
    }       
    else if(timezone >= 10)
    {
        LcdWriteTopLine('+', sizeof(_text)); 
        LcdWriteTopLine(buffer[0], sizeof(_text) + 1);
        LcdWriteTopLine(buffer[1], sizeof(_text) + 2);               
    }
    else
    {        
        LcdWriteTopLine(buffer[0], sizeof(_text));
        LcdWriteTopLine(buffer[1], sizeof(_text) + 1);
        LcdWriteTopLine(buffer[2], sizeof(_text) + 2);
    }       
}

/* 
 * \AUTHOR Tycho
 */
void LCDSetBottomLine(char *input)
{   
    if(input != bottomline)
    {
        bottomline = input;    
        bottomcount = 0;
    }
}

/* 
 * \AUTHOR Tycho
 */
void LcdUpdate() 
{
    char scrollBottomTemp[16];
        
    LcdClearBottomLine();
    strncpy(&scrollBottomTemp[0], &bottomline[bottomcount], 16);

    if (strlen(bottomline) > 16)
        bottomcount++;

    writeBottomLine(scrollBottomTemp);

    if (bottomcount + 16 > strlen(bottomline)) 
    {
        bottomcount = 0;
    }
}

/* 
 * \AUTHOR Tycho
 */
void writeBottomLine(char *input)
{
    int i;
    for(i=0; i < strlen(input); i++)
        {
            LcdWriteByte(WRITE_COMMAND, FIRSTPOS_LINE_1 + i);
            LcdWriteByte(WRITE_DATA, input[i]);
        }
}


/* ---------- end of module ------------------------------------------------ */

/*@}*/

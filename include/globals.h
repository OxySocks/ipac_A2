/* 
 * File:   globals.h
 * Author: Tim
 *
 * Created on 26 februari 2013, 13:32
 */

#define SYNC_LAST10MIN                  0
#define SYNC_LASTHOUR                   1
#define SYNC_DELAYED                    2

#define ALARM_DISABLED                  0
#define ALARM_ENABLED                   1
#define ALARM_SNOOZED                   2

#define VIEW_WELCOME_MESSAGE            0
#define VIEW_SET_TIMEZONE               1
#define VIEW_MAIN_DISPLAY               2

#define MENU_CLOSED                     0
#define MENU_SETTINGS                   1
#define MENU_EXTRA                      2 

#define MENU_RSS                        30
#define MENU_DISPLAY_RSS                31
#define MENU_WEBDATA                    32
#define MENU_INIT_INTERNET              3
#define MENU_TIMEZONE                   4
#define MENU_TIMEZONE_SET               5

#define MENU_ALARM1                     6
#define MENU_ALARM1_TIME                7
#define MENU_ALARM1_DATE                8
#define MENU_ALARM1_SNOOZE              9
#define MENU_ALARM1_SOUND               10

#define MENU_ALARM2                     20
#define MENU_ALARM2_TIME                21
#define MENU_ALARM2_DATE                22
#define MENU_ALARM2_SNOOZE              23
#define MENU_ALARM2_SOUND               24

#define EXTRA_FALLINGASLEEP             25
#define MENU_ALARM_SELECTED_1          1
#define MENU_ALARM_SELECTED_2          2
#define MENU_ALARM_SELECTED_3          3

#define ALARM_1                         0 
#define ALARM_2                         1



#define MAX_VOLUME                      60

int SYNC_STATUS;

int ALARM1_STATUS;
int ALARM2_STATUS;

int SELECTED_MENU;
int MENU_ALARM_SELECTED_TIME;

int CURRENT_VIEW;
int timezone;

int ALARM1_TIME_HOURS;
int ALARM1_TIME_MIN;
int ALARM1_TIME_SEC;

int ALARM1_DATE_DAY;
int ALARM1_DATE_MONTH;
int ALARM1_DATE_YEAR;

int ALARM1_SNOOZE;

int ALARM2_TIME_HOURS;
int ALARM2_TIME_MIN;
int ALARM2_TIME_SEC;

int ALARM2_DATE_DAY;
int ALARM2_DATE_MONTH;
int ALARM2_DATE_YEAR;

int ALARM2_SNOOZE;

char RSS_DATA[1000];
int ALARM1_STREAM_CHOICE;
int ALARM2_STREAM_CHOICE;


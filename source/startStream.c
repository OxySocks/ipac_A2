
 /*! startstream.c is supposed to initialize a working shoutcaststream
 *  
 * \AUTHOR Vincent (meent dit gemaakt te hebben).
 */
 
#define LOG_MODULE  LOG_MAIN_MODULE

/*--------------------------------------------------------------------------*/
/*  Include files                                                           */
/*--------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include <io.h>
#include <sys/thread.h>
#include <sys/timer.h>
#include <sys/version.h>
#include <dev/irqreg.h>

#include "processStream.h"
#include "startStream.h"
#include "display.h"

TCPSOCKET* SocketCreate(TCPSOCKET *sock,u_short mss,u_long rx_to,u_short tcpbufsiz);
int is_running = 0;
/* contains the args for mp3streamthread*/
typedef struct {
        char *name;
        FILE *stream;
        u_long metaint;
        int sleeptime;
}thread_args;

/* Stuff for the internet connection*/
TCPSOCKET *sock;
FILE *stream;
u_long radio_ip;
u_short tcpbufsiz = TCPIP_BUFSIZ;
u_long rx_to = TCPIP_READTIMEOUT;
u_short mss = TCPIP_MSS;
u_long metaint;

THREAD(PlayMp3StreamThread, arg)
{
	u_short mss = 1460;
	u_long rx_to = 3000;
	u_short tcpbufsiz = 8000;
	FILE *stream;
	sock = SocketCreate(sock,mss,rx_to,tcpbufsiz);
	
	/*Connect the radio station.*/
        u_long radio_ip = inet_addr(RADIO_IPADDR);
        stream = ConnectStation(sock, radio_ip, RADIO_PORT, &metaint);
	
	if(stream)
	{
		PlayMp3Stream(stream,metaint);
		fclose(stream);
        }
        NutTcpCloseSocket(sock);
        NutThreadExit();
        for (;;);
}

TCPSOCKET* SocketCreate(TCPSOCKET *sock,u_short mss,u_long rx_to,u_short tcpbufsiz)
{
        if ((sock = NutTcpCreateSocket()) == 0)
        {
                puts("Error: Can't create socket");
               for(;;);
        }

        /*Set socket options. Failures are ignored.*/
        if (NutTcpSetSockOpt(sock, TCP_MAXSEG, &mss, sizeof(mss)))
                printf("Sockopt MSS failed\n");
        if (NutTcpSetSockOpt(sock, SO_RCVTIMEO, &rx_to, sizeof(rx_to)))
                printf("Sockopt TO failed\n");
        if (NutTcpSetSockOpt(sock, SO_RCVBUF, &tcpbufsiz, sizeof(tcpbufsiz)))
                printf("Sockopt rxbuf failed\n");
        return sock;
}
	
int start_playing_radio2()
{
        RADIO_IPADDR = "145.58.52.145";
        RADIO_PORT = 80;
        RADIO_URL = "/radio2-bb-mp3";
        if(is_running == 0 )
        {
		is_running = 1;
		playing = 1;
                
                NutThreadCreate("mp3Stream" , PlayMp3StreamThread, NULL, 256);
	}
	else if(is_running == 1)
	{
		printf("Already running + %s \n", Description );
		return 0;
	}
        return 0;
}
int start_playing_JPOP()
{
        RADIO_IPADDR = "173.192.48.97";
        RADIO_PORT = 8479;
        RADIO_URL = "/";
        if(is_running == 0 )
        {
		is_running = 1;
		playing = 1;
                
                NutThreadCreate("mp3Stream" , PlayMp3StreamThread, NULL, 256);
	}
	else if(is_running == 1)
	{
		printf("Already running + %s\n", Description );
		return 0;
	}
        return 0;
}

void stop_stream()
{      
        //LCDSetBottomLine("Stream is stopping...");
        is_running = 0;
        playing = 0;  
}
